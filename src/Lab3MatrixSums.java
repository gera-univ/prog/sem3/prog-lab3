import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.DoubleStream;

public class Lab3MatrixSums {

    private static int N = 0;

    public static void main(String[] args) {
        ProcessArguments(args);

        double[][] matrix = new double[N][N];

        FillMatrixWithRandomNumbers(matrix, -N, N);

        System.out.println("Unsorted:");
        PrintMatrixElementsAndSums(matrix);

        Arrays.sort(matrix, Comparator.comparingDouble(a -> DoubleStream.of(a).sum()));

        System.out.println("Sorted by sum:");
        PrintMatrixElementsAndSums(matrix);
    }

    public static void ProcessArguments(String[] arguments) {
        try {
            N = Integer.parseInt(arguments[0]);
        } catch (ArrayIndexOutOfBoundsException e) {
            PrintUsageAndExit();
        } catch (NumberFormatException e) {
            System.out.println("Integer parameter expected.");
            PrintUsageAndExit();
        }
    }

    private static void PrintUsageAndExit() {
        System.out.printf("Usage: java %s [dimensions]\n", Lab3MatrixSums.class.getCanonicalName());
        System.exit(-1);
    }

    private static void FillMatrixWithRandomNumbers(double[][] matrix, double from, double to) {
        double min = Math.min(from, to);
        double max = Math.max(from, to);
        for (int row = 0; row < matrix.length; ++row)
            for (int col = 0; col < matrix[0].length; ++col)
                matrix[row][col] = ThreadLocalRandom.current().nextDouble(min, max); // java 7
    }

    private static void PrintMatrixElementsAndSums(double[][] matrix) {
        for (double[] row : matrix) {
            double sum = 0.0;
            for (double e : row) {
                sum += e;
                System.out.printf("%f\t", e);
            }
            System.out.printf("\t\t\u2211=%f\n", sum);
        }
    }
}
